<?php

require_once ( __DIR__ . '/../src/CodeWars.php');
use PHPUnit\Framework\TestCase;

class CodeWarsTest extends TestCase {

    public function testLongestConsec()
    {
        $this->assertEquals(app\CodeWars::longestConsec(["zone", "abigail", "theta", "form", "libe", "zas"], 2), "abigailtheta");
        $this->assertEquals(app\CodeWars::longestConsec(["ejjjjmmtthh", "zxxuueeg", "aanlljrrrxx", "dqqqaaabbb", "oocccffuucccjjjkkkjyyyeehh"], 1), "oocccffuucccjjjkkkjyyyeehh");
        $this->assertEquals(app\CodeWars::longestConsec([], 3), "");
    }
}