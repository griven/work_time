<?php

require_once ( __DIR__ . '/../src/Money.php');
use PHPUnit\Framework\TestCase;

class MoneyTest extends TestCase {

    public function testForTest()
    {
        $this->assertEquals('123', '123');
    }

    public function testForTest2()
    {
        $money = new app\Money();
        $this->assertEquals($money->forTest(113), '123');
    }
}