<?php

$ts = $argv[1];

$dt = new DateTime();

echo $dt->setTimestamp($ts)->format('d-m-y h-i-s');
echo PHP_EOL;