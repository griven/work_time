<?php

CONST FILES_DIR = __DIR__ . '/../files/sitemap/';
CONST OUTPUT_FILE = FILES_DIR . 'output.log';

multiCurl(getUrls());

function getUrls()
{
    $urls = [];

    for ($i = 0; $i < 12; $i++) {
        $DomDocument = new DOMDocument();
        $DomDocument->preserveWhiteSpace = false;
        $DomDocument->load(FILES_DIR . 'sitemap' . $i . '.xml');
        $DomNodeList = $DomDocument->getElementsByTagName('loc');

        foreach ($DomNodeList as $url) {
            $urls[] = $url->nodeValue;
        }
    }

    return $urls;
}

function multiCurl(array $urls)
{
    $countUrls = count($urls);
    $limit = 1000;
    $timeout = 30;
    $j=0;
    file_put_contents(OUTPUT_FILE, "Begin\n\n");
    $startTime = microtime(true);
    $codeInfo = [];

    for ($i = $countUrls; $i > 0; $i -= $limit) {
        $limit = ($i > $limit) ? $limit : $i;
        echo "\n Remain : $i, limit : $limit \n";
        $url = array_slice($urls, $j++ * $limit, $limit);

        // Setando opção padrão para todas url e adicionando a fila para processamento
        $mh = curl_multi_init();
        foreach ($url as $key => $value) {
            $ch[$key] = curl_init($value);
            curl_setopt($ch[$key], CURLOPT_NOBODY, true);
            curl_setopt($ch[$key], CURLOPT_HEADER, true);
            curl_setopt($ch[$key], CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch[$key], CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch[$key], CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($ch[$key], CURLOPT_CONNECTTIMEOUT, $timeout);

            curl_multi_add_handle($mh, $ch[$key]);
        }

        // Executando consulta
        do {
            curl_multi_exec($mh, $running);
        } while ($running > 0);

        // Obtendo dados de todas as consultas e retirando da fila
        $result = '';
        foreach (array_keys($ch) as $key) {
            $code = curl_getinfo($ch[$key], CURLINFO_HTTP_CODE);
            $urlString = curl_getinfo($ch[$key], CURLINFO_EFFECTIVE_URL) . "\n";

            $codeInfo[$code][] = $urlString;

            $result .= $code . " " . $urlString. "\n";

            curl_multi_remove_handle($mh, $ch[$key]);
        }

        $persentage = round(100 - 100 * (($i - $limit) / $countUrls), 1);
        $executeTime = microtime(true) - $startTime;
        $lastTime = (100 - $persentage) / ($persentage / $executeTime);

        $result .= "\nPercentage = $persentage \n";

        $debugData = "\nPercentage = $persentage \n";
        $debugData .= "Running sec = $executeTime \n";
        $debugData .= "Remain sec = $lastTime \n";

        $debugData .= "Code: \n";

        foreach ($codeInfo as $code => $info) {
            $debugData .=  '  ' . $code . ' - ' . count($info) . "\n";
        }

        echo $debugData;
        file_put_contents(OUTPUT_FILE, $result, FILE_APPEND);


        // Finalizando
        curl_multi_close($mh);
    }
}

