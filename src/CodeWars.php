<?php

namespace app;

class CodeWars
{
    public static function longestConsec($strarr, $k) {
        $longest = '';
        for($i=0; ($i+$k) <= count($strarr); ++$i) {
            $res = '';
            for($j = $i; $j < ($i+$k); $j++) {
                $res .= $strarr[$j];
            }
            $longest = (strlen($longest) >= strlen($res)) ? $longest : $res;
        }

        return $longest;
    }
}