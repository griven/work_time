<?php

if(!isset($argv[1]) || $argv[1] == '-h') {
    help();
} else {
    getResult($argv[1]);
}

function help()
{
    echo <<<END
Usage:
  php -f work_time.php [name]

END;
}

function getResult($name)
{
    $now = new DateTime();
    $comingTime = getCommingTime($name);
    if($comingTime) {
        $workInterval = DateInterval::createFromDateString('+9 hours');
        $finishTime = getCommingTime($name)->add($workInterval);
        $leftTime = $now->diff($finishTime);

        echo $now->format('M d');
        echo "\nПришел: ";
        echo $comingTime->format('H-i');
        echo "\nМожно уйти: ";
        echo $finishTime->format('H-i');
        echo "\nОсталось: ";
        echo $leftTime->format('%r%H-%i') . "\n";
    } else {
        echo "Не приходил сегодня\n";
    }
}

function getPage($url, $tmpFile)
{
    $ch = curl_init($url);
    $fp = fopen($tmpFile, "w");

    curl_setopt($ch, CURLOPT_FILE, $fp);
    curl_setopt($ch, CURLOPT_HEADER, 0);

    curl_exec($ch);
    curl_close($ch);
    fclose($fp);

    return file_get_contents($tmpFile);
}

function getCommingTime($name)
{
    $today = new DateTime();
    $dayNumber = $today->format('d');

    $pageStr = getPage('http://cserver.athr.ru', "/tmp/prohodka.txt");

    $flag = false;
    $i = 0;

    preg_match_all("<(.+)>", $pageStr, $tags);
    foreach ($tags[1] as $tag) {
        if (preg_match('/'. $name. '/', $tag)) {
            $flag = true;
            continue;
        }
        if ($flag && ($i < $dayNumber)) {
            $i++;
            $result = trim($tag);
        } elseif ($flag) {
            break;
        }
    }
    
    if(!empty($result)) {
        preg_match_all('/(\d\d)/', $result, $time);
    }
    
    if(empty($time[1])) {
        return false;
    }
    
    $comeTime = DateTime::createFromFormat('H:i', $time[1][0] . ':' . $time[1][1]);
    return $comeTime;
}